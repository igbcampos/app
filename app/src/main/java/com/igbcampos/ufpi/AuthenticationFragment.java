package com.igbcampos.ufpi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class AuthenticationFragment extends Fragment {

    public AuthenticationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_authentication, container, false);

        Button authenticationButton = (Button) view.findViewById(R.id.authenticate_button);
        authenticationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Utils utils = new Utils();
                utils.authenticate(view.getContext());*/
                Utils.stopWork();
                Utils.startWork();

                Toast.makeText(view.getContext(), "Autenticando...", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
