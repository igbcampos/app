package com.igbcampos.ufpi;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;

import java.io.IOException;

import androidx.work.Worker;

public class AuthenticationWorker extends Worker {
    //public static Context context =;
    //https://medium.com/android-dev-br/workmanager-o-que-%C3%A9-como-usar-26f5b800984e
    //https://imasters.com.br/android/workmanager-o-que-e-como-usar

    @NonNull
    @Override
    public Result doWork() {

        String user = getInputData().getString("user", "user");
        String password = getInputData().getString("password", "password");

        /*String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
                if(ssid == "UFPI") {
                    try {
                        Document document = null;
                        document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/logar.do?dispatch=logOn&user.login=" + user + "&user.senha=" + password).post();
                        sendNotification("Autenticando", user + ": " + password);

                        if(document != null) {
                            return Result.SUCCESS;
                        }
                        else {
                            return Result.RETRY;
                        }
                    } catch (IOException e) {
                        return Result.RETRY;
                    }
                }
                sendNotification("Não é UFPI", user + ": " + password);
            }
        }*/

        //Utils.readFile(context);
        /*Utils.Authenticator.execute();

        if(Utils.noConnection) {
            return Result.RETRY;
        }
        else {
            return Result.SUCCESS;
        }*/

        if(Utils.getWifiName().equals("UFPI")) {
            try {
                Document document = null;
                document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/logar.do?dispatch=logOn&user.login=" + user + "&user.senha=" + password).post();
                sendNotification("Autenticando", user + ": " + password);

                if(document == null) {
                    return Result.RETRY;
                }
                else if(document.getElementsByClass("form-signin-heading").size() == 1 && document.getElementsByClass("form-signin-heading").text().equals( "Autenticado com sucesso!")) {
                    return Result.SUCCESS;
                }
                else {
                    return Result.RETRY;
                }
            } catch (IOException e) {
                return Result.RETRY;
            }
        }
        else {
            sendNotification("Não é UFPI", user + ": " + password);
        }


        /*final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";

        try {
            Connection.Response loginFormResponse = Jsoup.connect("https://login.ufpi.br:6082/php/uid.php?vsys=1&rule=0&url=http://conecta.ufpi.br%2f")
                    .method(Connection.Method.GET)
                    .userAgent(USER_AGENT)
                    .execute();

            FormElement loginForm = (FormElement) loginFormResponse.parse()
                    .select("form#login_form").first();

            Log.i("Login Form", "" + loginForm.toString());

            Element loginField = loginForm.select("#user").first();
            loginField.val(user);

            Element passwordField = loginForm.select("#passwd").first();
            passwordField.val(password);

            Connection.Response loginActionResponse = loginForm.submit()
                    .cookies(loginFormResponse.cookies())
                    .userAgent(USER_AGENT)
                    .execute();

            Log.i("Autenticando", "" + loginActionResponse.parse().html());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        return Result.SUCCESS;
    }

    public void sendNotification(String title, String message) {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManager.notify(1, notification.build());
    }
}
