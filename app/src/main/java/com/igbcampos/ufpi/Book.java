package com.igbcampos.ufpi;

public class Book {

    private String name;
    private String author;
    private String library;
    private String loanDate;
    private String returnTerm;

    public Book(String name, String author, String library, String loanDate, String returnTerm) {
        this.name = name;
        this.author = author;
        this.library = library;
        this.loanDate = loanDate;
        this.returnTerm = returnTerm;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getLibrary() {
        return library;
    }

    public String getLoanDate() {
        return loanDate;
    }

    public String getReturnTerm() {
        return returnTerm;
    }
}
