package com.igbcampos.ufpi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    private Context context;
    private List<Book> booksList;

    public BookAdapter(Context context, List<Book> booksList) {
        this.context = context;
        this.booksList = booksList;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.book_layout, null);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        //getting the product of the specified position
        Book book = booksList.get(position);

        //binding the data with the viewholder views
        holder.nameTextView.setText(book.getName());
        holder.authorTextView.setText(book.getAuthor());
        holder.libraryTextView.setText(book.getLibrary());
        holder.loanDateTextView.setText("Empréstimo: " + book.getLoanDate());
        holder.returnTermTextView.setText("Prazo: " + book.getReturnTerm());
    }

    @Override
    public int getItemCount() {
        return booksList.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView authorTextView;
        TextView libraryTextView;
        TextView loanDateTextView;
        TextView returnTermTextView;

        public BookViewHolder(View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.book_layout_name);
            authorTextView = itemView.findViewById(R.id.book_layout_author);
            libraryTextView = itemView.findViewById(R.id.book_layout_library);
            loanDateTextView = itemView.findViewById(R.id.book_layout_loan_date);
            returnTermTextView = itemView.findViewById(R.id.book_layout_time_return_term);
        }

    }

}
