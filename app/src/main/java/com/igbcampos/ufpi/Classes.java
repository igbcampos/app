package com.igbcampos.ufpi;

public class Classes {

    private String id;
    private String name;
    private String place;
    private String time;

    public Classes(String id, String name, String place, String time) {
        this.id = id;
        this.name = name;
        this.place = place;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public String getTime() {
        return time;
    }
}