package com.igbcampos.ufpi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.ClassesViewHolder> {

    private Context context;
    private List<Classes> classesList;

    public ClassesAdapter(Context context, List<Classes> classesList) {
        this.context = context;
        this.classesList = classesList;
    }

    @NonNull
    @Override
    public ClassesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.class_layout, null);
        return new ClassesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassesViewHolder holder, int position) {
        //getting the product of the specified position
        Classes classes = classesList.get(position);

        //binding the data with the viewholder views
        holder.id = classes.getId();
        holder.nameTextView.setText(classes.getName());
        holder.placeTextView.setText("Local: " + classes.getPlace());
        holder.timeTextView.setText(String.valueOf("Horário: " + classes.getTime()));
    }

    @Override
    public int getItemCount() {
        return classesList.size();
    }

    class ClassesViewHolder extends RecyclerView.ViewHolder {

        String id;
        TextView nameTextView;
        TextView placeTextView;
        TextView timeTextView;

        public ClassesViewHolder(final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "id: " + id, Toast.LENGTH_SHORT).show();

                    Intent logged = new Intent(context, VirtualClassActivity.class);
                    context.startActivity(logged);

                    Utils.operation = "class";
                    Utils.classId = id;
                    new Utils();
                    //"https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?form_acessarTurmaVirtual=form_acessarTurmaVirtual&idTurma=" + id + "&javax.faces.ViewState=j_id1&form_acessarTurmaVirtual%3AturmaVirtual=form_acessarTurmaVirtual%3AturmaVirtual"
                }
            });

            nameTextView = itemView.findViewById(R.id.class_layout_name);
            placeTextView = itemView.findViewById(R.id.class_layout_place);
            timeTextView = itemView.findViewById(R.id.class_layout_time);
        }

    }

}
