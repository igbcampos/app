package com.igbcampos.ufpi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> studentGradesGroups;
    private HashMap<String, List<Grades>> studentGradesListItem;

    public ExpandableListAdapter(Context context, List<String> studenGradesGroups, HashMap<String, List<Grades>> studentGradesListItem) {
        this.context = context;
        this.studentGradesGroups = studenGradesGroups;
        this.studentGradesListItem = studentGradesListItem;
    }

    @Override
    public int getGroupCount() {
        return studentGradesGroups.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return studentGradesListItem.get(studentGradesGroups.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return studentGradesGroups.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return studentGradesListItem.get(studentGradesGroups.get(i)).get(i1 );
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1   ;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if(view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.student_grades_group, null);
        }

        TextView groupTextView = (TextView) view.findViewById(R.id.student_grades_group_name);
        groupTextView.setText((String) getGroup(i));

        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if(view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.student_grades_list_item, null);
        }

        TextView subjectTextView = (TextView) view.findViewById(R.id.student_grades_list_item_subject);
        TextView averageTextView = (TextView) view.findViewById(R.id.student_grades_list_item_average);
        TextView frequencyTextView = (TextView) view.findViewById(R.id.student_grades_list_item_frequency);
        TextView situationTextView = (TextView) view.findViewById(R.id.student_grades_list_item_situation);
        TextView firstGradeTextView = (TextView) view.findViewById(R.id.student_grades_list_item_first_grade);
        TextView secondGradeTextView = (TextView) view.findViewById(R.id.student_grades_list_item_second_grade);
        TextView thirdGradeTextView = (TextView) view.findViewById(R.id.student_grades_list_item_third_grade);
        TextView fourthGradeTextView = (TextView) view.findViewById(R.id.student_grades_list_item_fourth_grade);
        TextView fifthGradeTextView = (TextView) view.findViewById(R.id.student_grades_list_item_fifth_grade);
        TextView finalExamGradeTextView = (TextView) view.findViewById(R.id.student_grades_list_item_final_exam_grade);

        Grades grades = (Grades) getChild(i, i1);

        if(grades.getSubject().equals("")) {
            subjectTextView.setVisibility(View.GONE);
        } else {
            subjectTextView.setVisibility(View.VISIBLE);
            subjectTextView.setText(grades.getSubject());
        }

        if(grades.getAverage().equals("")) {
            averageTextView.setVisibility(View.GONE);
        } else {
            averageTextView.setVisibility(View.VISIBLE);
            averageTextView.setText("Resultado: " + grades.getAverage());
        }

        if(grades.getFrequency().equals("")) {
            frequencyTextView.setVisibility(View.GONE);
        } else {
            frequencyTextView.setVisibility(View.VISIBLE);
            frequencyTextView.setText("Faltas: " + grades.getFrequency());
        }

        if(grades.getSituation().equals("")) {
            situationTextView.setVisibility(View.GONE);
        } else {
            situationTextView.setVisibility(View.VISIBLE);
            situationTextView.setText("Situação: " + grades.getSituation());
        }

        if(grades.getFirstGrade().equals("")) {
            firstGradeTextView.setVisibility(View.GONE);
        } else {
            firstGradeTextView.setVisibility(View.VISIBLE);
            firstGradeTextView.setText("Un. 1: " + grades.getFirstGrade());
        }

        if(grades.getSecondGrade().equals("")) {
            secondGradeTextView.setVisibility(View.GONE);
        } else {
            secondGradeTextView.setVisibility(View.VISIBLE);
            secondGradeTextView.setText("Un. 2: " + grades.getSecondGrade());
        }

        if(grades.getThirdGrade().equals("")) {
            thirdGradeTextView.setVisibility(View.GONE);
        } else {
            thirdGradeTextView.setVisibility(View.VISIBLE);
            thirdGradeTextView.setText("Un. 3: " + grades.getThirdGrade());
        }

        if(grades.getFourthGrade().equals("")) {
            fourthGradeTextView.setVisibility(View.GONE);
        } else {
            fourthGradeTextView.setVisibility(View.VISIBLE);
            fourthGradeTextView.setText("Un. 4: " + grades.getFourthGrade());
        }

        if(grades.getFifthGrade().equals("")) {
            fifthGradeTextView.setVisibility(View.GONE);
        } else {
            fifthGradeTextView.setVisibility(View.VISIBLE);
            fifthGradeTextView.setText("Un. 5: " + grades.getFifthGrade());
        }

        if(grades.getFinalExamGrade().equals("")) {
            finalExamGradeTextView.setVisibility(View.GONE);
        } else {
            finalExamGradeTextView.setVisibility(View.VISIBLE);
            finalExamGradeTextView.setText("Exame Final: " + grades.getFinalExamGrade());
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
