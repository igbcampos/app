package com.igbcampos.ufpi;

public class Grades {

    private String subject;
    private String firstGrade;
    private String secondGrade;
    private String thirdGrade;
    private String fourthGrade;
    private String fifthGrade;
    private String finalExamGrade;
    private String average;
    private String frequency;
    private String situation;

    public Grades() {}

    public Grades(String subject, String firstGrade, String secondGrade, String thirdGrade, String fourthGrade, String fifthGrade, String finalExamGrade, String average, String frequency, String situation) {
        this.subject = subject;
        this.firstGrade = firstGrade;
        this.secondGrade = secondGrade;
        this.thirdGrade = thirdGrade;
        this.fourthGrade = fourthGrade;
        this.fifthGrade = fifthGrade;
        this.finalExamGrade = finalExamGrade;
        this.average = average;
        this.frequency = frequency;
        this.situation = situation;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFirstGrade() {
        return firstGrade;
    }

    public void setFirstGrade(String firstGrade) {
        this.firstGrade = firstGrade;
    }

    public String getSecondGrade() {
        return secondGrade;
    }

    public void setSecondGrade(String secondGrade) {
        this.secondGrade = secondGrade;
    }

    public String getThirdGrade() {
        return thirdGrade;
    }

    public void setThirdGrade(String thirdGrade) {
        this.thirdGrade = thirdGrade;
    }

    public String getFourthGrade() {
        return fourthGrade;
    }

    public void setFourthGrade(String fourthGrade) {
        this.fourthGrade = fourthGrade;
    }

    public String getFifthGrade() {
        return fifthGrade;
    }

    public void setFifthGrade(String fifthGrade) {
        this.fifthGrade = fifthGrade;
    }

    public String getFinalExamGrade() {
        return finalExamGrade;
    }

    public void setFinalExamGrade(String finalExamGrade) {
        this.finalExamGrade = finalExamGrade;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }
}
