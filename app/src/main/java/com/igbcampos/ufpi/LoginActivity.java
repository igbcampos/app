package com.igbcampos.ufpi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        File file = new File(getFilesDir(), Utils.fileName);
        if(file.exists()) {
            Intent logged = new Intent(getApplicationContext(), StudentMenuActivity.class);
            startActivity(logged);
            finish();
        }
    }

    public void login(View view) {
        //Toast.makeText(this, "Logando...", Toast.LENGTH_LONG).show();
        EditText userEditText = (EditText) findViewById(R.id.user);
        String user = userEditText.getText().toString();

        EditText passwordEditText = (EditText) findViewById(R.id.password);
        String password = passwordEditText.getText().toString();

        if(TextUtils.isEmpty(userEditText.getText())) {
            userEditText.setError("Digite o usuário!");
        }
        else if(TextUtils.isEmpty(passwordEditText.getText())) {
            passwordEditText.setError("Digite a senha!");
        }
        else {
            String content = "" + user + "-%" + password;
            Utils.createFile(this, content);

            new Utils(this);

            //Log.i("Result", "" + ((!Utils.invalidInfo) && (!Utils.noConnection)));
            if((!Utils.invalidInfo) && (!Utils.noConnection)) {
                Log.i("Login", "Starting work.");
                Utils.startWork();

                new Utils().sendData(this);

                Intent logged = new Intent(getApplicationContext(), StudentMenuActivity.class);
                startActivity(logged);
                finish();
            }
        }
    }
}
