package com.igbcampos.ufpi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.nodes.Element;
import org.w3c.dom.Text;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentBondFragment extends Fragment {

    public StudentBondFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_bond, container, false);
        ListView bondsListView = (ListView) view.findViewById(R.id.student_bond_fragment_list);

        Utils.operation = "bond";
        new Utils();

        for(Element status : Utils.requestedPage.getElementsByClass("subFormulario")) {
            for(Element line : status.child(2).children()) {
                if(line.children().size() > 0) {
                    String link = line.child(0).child(0).attr("href");
                    String bond = line.child(1).child(0).text();
                    String id = line.child(2).child(0).text();
                    String activate = line.child(3).child(0).text();
                    String major = line.child(4).child(0).text();

                    Log.i("Bond", link + ", " + bond + ", " + id + ", " + activate + ", " + major);

                    View cardView = inflater.inflate(R.layout.bond_layout, null);

                    TextView majorTextView = (TextView) cardView.findViewById(R.id.bond_layout_major);
                    majorTextView.setText(major);

                    TextView idTextView = (TextView) cardView.findViewById(R.id.bond_layout_id);
                    idTextView.setText(major);

                    TextView bondTextView = (TextView) cardView.findViewById(R.id.bond_layout_bond);
                    bondTextView.setText(major);

                    TextView activateTextView = (TextView) cardView.findViewById(R.id.bond_layout_activate);
                    activateTextView.setText(major);

                    bondsListView.addView(cardView);
                }
                else {
                    Log.i("Bond", "Linha vazia.");
                }
            }
        }

        return view;
    }

}
