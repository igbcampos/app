package com.igbcampos.ufpi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentClassesFragment extends Fragment {

    public StudentClassesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_classes, container, false);
        TextView newsTextView = (TextView) view.findViewById(R.id.student_classes_fragment_news);

        if(Utils.homepage.getElementsByClass("vazio").size() > 0 /*&& Utils.homepage.getElementsByClass("vazio").first().text().equals("Nenhuma turma neste semestre")*/) {
            newsTextView.setText("Nenhuma turma neste semestre");
        }
        else {
            newsTextView.setVisibility(View.GONE);

            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.student_classes_fragment_recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

            //initializing the productlist
            List<Classes> classesList = new ArrayList<>();

            for(Element classes : Utils.homepage.getElementById("turmas-portal").child(1).child(1).children()) {
                if(classes.children().size() > 1) {
                    String id = classes.child(0).child(0).child(1).attr("value");
                    String name = classes.child(0).child(0).child(2).text();
                    String place = classes.child(1).text();
                    String time = classes.child(2).child(0).text();

                    classesList.add(new Classes(id, name, place, time));
                }
            }

            //creating recyclerview adapter
            ClassesAdapter adapter = new ClassesAdapter(view.getContext(), classesList);

            //setting adapter to recyclerview
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

}
