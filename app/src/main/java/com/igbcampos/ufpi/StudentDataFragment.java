package com.igbcampos.ufpi;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentDataFragment extends Fragment {

    public StudentDataFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_data, container, false);

        if(Utils.homepage != null) {
            Document document = Utils.homepage;

            String pictureUrl = document.getElementsByTag("img").attr("src");
            String name = document.getElementsByClass("nome").first().child(0).child(0).text();
            String ira = "IRA: " + document.getElementsByTag("acronym").parents().parents().first().child(1).child(0).text();

            Element personalData = document.getElementById("agenda-docente").parent().parent().child(5).child(1).child(0);

            String id = "Matrícula: " + personalData.child(0).child(1).text();
            String major = "Curso: " + personalData.child(1).child(1).text().split(" - ")[1];
            String shift = "Turno: " + personalData.child(2).child(1).text().split(" \\(")[0];
            String level = "Nível: " + personalData.child(3).child(1).text();
            String status = "Status: " + personalData.child(4).child(1).text();
            //String email = "E-mail: " + personalData.child(5).child(1).text();
            String entry = "Entrada: " + personalData.child(6).child(1).text();

            Uri imageUri = Uri.parse("https://sigaa.ufpi.br/" + pictureUrl);
            SimpleDraweeView draweeView = (SimpleDraweeView) view.findViewById(R.id.student_image);
            draweeView.setImageURI(imageUri);

            TextView nameTextView = (TextView) view.findViewById(R.id.data_fragment_student_name);
            nameTextView.setText(name);

            TextView userTextView = (TextView) view.findViewById(R.id.data_fragment_student_user);
            userTextView.setText("@" + Utils.user);

            TextView iraTextView = (TextView) view.findViewById(R.id.data_fragment_student_ira);
            iraTextView.setText(ira);

            TextView idTextView = (TextView) view.findViewById(R.id.data_fragment_student_id);
            idTextView.setText(id);

            TextView majorTextView = (TextView) view.findViewById(R.id.data_fragment_student_major);
            majorTextView.setText(major);

            TextView shiftTextView = (TextView) view.findViewById(R.id.data_fragment_student_shift);
            shiftTextView.setText(shift);

            TextView levelTextView = (TextView) view.findViewById(R.id.data_fragment_student_level);
            levelTextView.setText(level);

            TextView statusTextView = (TextView) view.findViewById(R.id.data_fragment_student_status);
            statusTextView.setText(status);

            TextView emailTextView = (TextView) view.findViewById(R.id.data_fragment_student_email);
            //emailTextView.setText(email);
            emailTextView.setVisibility(View.GONE);

            TextView entryTextView = (TextView) view.findViewById(R.id.data_fragment_student_entry);
            entryTextView.setText(entry);

            Button bondButton = (Button) view.findViewById(R.id.data_fragment_student_alter_bond);

            if(document.getElementsByClass("usuario").first().children().size() == 1) {
                bondButton.setVisibility(View.GONE);
            }
            else {
                bondButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(view, "Se acalme, vai funcionar.", Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        }

        return view;
    }

}
