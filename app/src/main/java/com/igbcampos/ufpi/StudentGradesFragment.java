package com.igbcampos.ufpi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentGradesFragment extends Fragment {

    public StudentGradesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_grades, container, false);
        TextView newsTextView = (TextView) view.findViewById(R.id.student_grades_fragment_news);
        //String data = "";

        Utils.operation = "grades";
        new Utils();

        if(Utils.requestedPage.getElementsByClass("tabelaRelatorio").size() > 0) {
            newsTextView.setVisibility(View.GONE);

            ExpandableListView expandableListView = (ExpandableListView) view.findViewById(R.id.student_grades_fragment_expandable_list_view);
            List<String> studentGradesGroups = new ArrayList<>();
            HashMap<String, List<Grades>> studentGradesListItem = new HashMap<>();

            for(Element semester : Utils.requestedPage.getElementsByClass("tabelaRelatorio")) {
                //data += table.text();
                String term = semester.getElementsByTag("caption").text();
                studentGradesGroups.add(term);

                List<Grades> studenGradesItems = new ArrayList<>();
                //gets the body of the table that represents the semester grades
                Elements body = semester.getElementsByTag("tbody").first().children();

                for (Element subject : body) {
                    Grades grades = new Grades();

                    grades.setSubject(subject.child(1).text());
                    grades.setFirstGrade(subject.child(2).text());
                    grades.setSecondGrade(subject.child(3).text());
                    grades.setThirdGrade(subject.child(4).text());
                    grades.setFourthGrade(subject.child(5).text());
                    grades.setFifthGrade(subject.child(6).text());
                    grades.setFinalExamGrade(subject.child(7).text());
                    grades.setAverage(subject.child(8).text());
                    grades.setFrequency(subject.child(9).text());
                    grades.setSituation(subject.child(10).text());

                    studenGradesItems.add(grades);
                }

                studentGradesListItem.put(term, studenGradesItems);
            }
            //newsTextView.setText(data);

            // cria um adaptador (BaseExpandableListAdapter) com os dados acima
            ExpandableListAdapter expandableListAdapter = new ExpandableListAdapter(getContext(), studentGradesGroups, studentGradesListItem);
            // define o apadtador do ExpandableListView
            expandableListView.setAdapter(expandableListAdapter);
        }
        else {
            newsTextView.setText("Você não possui notas cadastradas.");
        }

        return view;
    }

}
