package com.igbcampos.ufpi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentLibraryFragment extends Fragment {

    public StudentLibraryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_library, container, false);
        TextView newsTextView = (TextView) view.findViewById(R.id.student_library_fragment_news);

        Utils.operation = "library";
        new Utils();

        if(Utils.requestedPage.getElementsByClass("warning").size() > 0 && Utils.requestedPage.getElementsByClass("warning").first().child(0).text().equals("Você não possui empréstimos ativos renováveis.")) {
            newsTextView.setText("Você não possui empréstimos ativos renováveis.");
        }
        else {
            newsTextView.setVisibility(View.GONE);

            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.student_library_fragment_recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

            //initializing the productlist
            List<Book> booksList = new ArrayList<>();

            for(Element book : Utils.requestedPage.getElementsByClass("listagem").first().child(2).children()) {
                String name = book.child(1).text().split(" - ")[2];
                String author = book.child(1).text().split(" - ")[1];
                String library = book.child(1).text().split(" - ")[3];
                String loanDate = book.child(2).text();
                String returnTerm = book.child(3).text();

                booksList.add(new Book(name, author, library, loanDate, returnTerm));
            }

            //creating recyclerview adapter
            BookAdapter adapter = new BookAdapter(view.getContext(), booksList);

            //setting adapter to recyclerview
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

}
