package com.igbcampos.ufpi;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import org.jsoup.nodes.Document;

public class StudentMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_student_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Utils.operation = "home";
        new Utils(this);

        View headerView = navigationView.getHeaderView(0);

        if(Utils.homepage != null) {
            Document document = Utils.homepage;

            String pictureUrl = document.getElementsByTag("img").attr("src");
            String name = document.getElementsByClass("nome").first().child(0).child(0).text();

            Uri imageUri = Uri.parse("https://sigaa.ufpi.br/" + pictureUrl);
            SimpleDraweeView draweeView = (SimpleDraweeView) headerView.findViewById(R.id.student_image);
            draweeView.setImageURI(imageUri);

            TextView nameTextView = (TextView) headerView.findViewById(R.id.student_name);
            nameTextView.setText(name);

            TextView userTextView = (TextView) headerView.findViewById(R.id.student_user);
            userTextView.setText("@" + Utils.user);

            Fragment fragment = new StudentClassesFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_student_menu, fragment);
            fragmentTransaction.commit();

            toolbar.setTitle("Turmas Virtuais");
        }

        /*Utils.operation = "bond";
        new Utils(this);*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.student_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_data) {
            fragment = new StudentDataFragment();
            toolbar.setTitle("Dados Pessoais");
        } else if (id == R.id.nav_classes) {
            fragment = new StudentClassesFragment();
            toolbar.setTitle("Turmas Virtuais");
        } else if (id == R.id.nav_grades) {
            fragment = new StudentGradesFragment();
            toolbar.setTitle("Notas");
        } else if (id == R.id.nav_schedule) {
            fragment = new StudentScheduleFragment();
            toolbar.setTitle("Horários");
        } else if (id == R.id.nav_library) {
            fragment = new StudentLibraryFragment();
            toolbar.setTitle("Biblioteca");
        } else if (id == R.id.nav_authentication) {
            fragment = new AuthenticationFragment();
            toolbar.setTitle("Autenticação");
        } else if (id == R.id.nav_logout) {
            Utils.stopWork();
            Utils.deleteFile(this);

            Intent login = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(login);
            finish();
        }

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_student_menu, fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
