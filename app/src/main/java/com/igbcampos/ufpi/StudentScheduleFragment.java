package com.igbcampos.ufpi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentScheduleFragment extends Fragment {

    private String script;

    public StudentScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_schedule, container, false);
        TextView newsTextView = (TextView) view.findViewById(R.id.student_schedule_fragment_news);
        Map<String, String> subjects = new HashMap<>();

        Utils.operation = "schedule";
        new Utils();

        //Log.i("Schedule", Utils.requestedPage.toString());

        if(Utils.requestedPage.getElementsByClass("erros").size() > 0 && Utils.requestedPage.getElementsByClass("erros").first().child(0).text().equals("O discente selecionado não possui matrículas em componentes do período atual.")) {
            newsTextView.setText("Você não possui matrículas em componentes do período atual.");
        }
        else {
            newsTextView.setVisibility(View.GONE);

            /*
            <ScrollView
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:id="@+id/schedule_info"/>

            </ScrollView>
            TextView infoTextView = (TextView) view.findViewById(R.id.schedule_info);
            infoTextView.setText(Utils.requestedPage.toString());*/

            for(Element subject : Utils.requestedPage.getElementById("matriculas").child(1).children()) {
                String subjectId = subject.child(0).text();
                String subjectDescription = subject.child(1).child(0).text();
                //Log.i("Class name", subjectDescription);
                subjects.put(subjectId, subjectDescription);
            }

            script = Utils.requestedPage.getElementsByTag("script").get(4).toString();

            TableRow tableRow = view.findViewById(R.id.student_schedule_fragment_m1);
            fillLine(1, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m2);
            fillLine(2, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m3);
            fillLine(3, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m4);
            fillLine(4, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m5);
            fillLine(5, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m6);
            fillLine(6, subjects, tableRow, view);

            tableRow = view.findViewById(R.id.student_schedule_fragment_t1);
            fillLine(8, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t2);
            fillLine(9, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t3);
            fillLine(10, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t4);
            fillLine(11, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t5);
            fillLine(12, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t6);
            fillLine(13, subjects, tableRow, view);

            tableRow = view.findViewById(R.id.student_schedule_fragment_n1);
            fillLine(15, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n2);
            fillLine(16, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n3);
            fillLine(17, subjects, tableRow, view);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n4);
            fillLine(18, subjects, tableRow, view);

            /*newsTextView.setVisibility(View.GONE);

            for(Element subject : Utils.requestedPage.getElementsByClass("listagem").first().child(2).children()) {
                String subjectDescription = subject.child(0).text();
                //Log.i("Class name", subjectDescription);
                subjects.put(subjectDescription.split("-")[0], subjectDescription.split("-")[1]);
                //subjects.put(subjectDescription.split(" - ")[0], "Teste");
            }*/

            /*for(Element row : Utils.requestedPage.getElementsByClass("formulario").first().child(0).children()) {
                if(row.attr("style").equals("font-size: 9px;")) {
                    for(Element column : row.children()) {

                        TextView columnTextView = new TextView(getContext());

                        if(column.children() != null) {
                            String value = column.child(0).text();

                            if(value != "---") {
                                value = subjects.get(value);
                            }

                            columnTextView.setText(value);
                        }
                    }
                }
            }*/

            /*Element row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(1);
            TableRow tableRow = view.findViewById(R.id.student_schedule_fragment_m1);
            fillLine(1, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(2);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m2);
            fillLine(2, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(3);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m3);
            fillLine(3, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(4);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m4);
            fillLine(4, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(5);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m5);
            fillLine(5, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(6);
            tableRow = view.findViewById(R.id.student_schedule_fragment_m6);
            fillLine(6, row, subjects, tableRow);

            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(8);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t1);
            fillLine(8, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(9);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t2);
            fillLine(9, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(10);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t3);
            fillLine(10, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(11);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t4);
            fillLine(11, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(12);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t5);
            fillLine(12, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(13);
            tableRow = view.findViewById(R.id.student_schedule_fragment_t6);
            fillLine(13, row, subjects, tableRow);

            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(15);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n1);
            fillLine(15, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(16);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n2);
            fillLine(16, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(17);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n3);
            fillLine(17, row, subjects, tableRow);
            row = Utils.requestedPage.getElementsByClass("formulario").first().child(0).child(18);
            tableRow = view.findViewById(R.id.student_schedule_fragment_n4);
            fillLine(18, row, subjects, tableRow);*/
        }

        return view;
    }

    public void fillLine(int i, Map<String, String> subjects, TableRow tableRow, View view) {
        Element row = Utils.requestedPage.getElementById("horario").child(0).child(i);

        for(Element column : row.children()) {
            TextView columnTextView = new TextView(view.getContext());

            if(column.children().size() > 0) {
                String id = column.child(0).attr("id");

                String value = column.child(0).text();

                int index = script.indexOf(id);
                if(index != -1) {
                    index = script.indexOf("\'", index + 8) + 1;
                    value = script.substring(index, script.indexOf("\'", index));
                }

                if(!value.equals("---")) {
                    value = subjects.get(value).split(" ")[0];
                }

                columnTextView.setText(value);
                columnTextView.setGravity(Gravity.CENTER);
                /*columnTextView.setLayoutParams(new TableLayout.LayoutParams(0,
                        TableLayout.LayoutParams.WRAP_CONTENT, 1f));*/
                columnTextView.setWidth(((View) tableRow.getParent()).getWidth() / 6);
                tableRow.addView(columnTextView);
            }
        }
    }

}
