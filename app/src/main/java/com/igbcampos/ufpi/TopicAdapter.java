package com.igbcampos.ufpi;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicViewHolder> {

    private Context context;
    private List<Topic> topicsList;
    public String formAva;
    public String id;
    public String javaxFaces;

    DownloadManager fileDownloadManager;
    long queueId;

    public TopicAdapter(Context context, List<Topic> topicsList) {
        this.context = context;
        this.topicsList = topicsList;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.topic_layout, null);
        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder holder, int position) {
        //getting the product of the specified position
        Topic topic = topicsList.get(position);

        //binding the data with the viewholder views
        holder.nameTextView.setText(topic.getName());

        Document document = Utils.requestedPage;
        javaxFaces = document.getElementById("javax.faces.ViewState").attr("value");
        Element topicItem = document.getElementsByClass("topico-aula").get(position);

        for(final Element topicContent : topicItem.getElementsByClass("conteudotopico").first().children()) {
            if(topicContent.tagName() == "p") {
                TextView infoTextView = new TextView(context);
                infoTextView.setText(topicContent.text());
                infoTextView.setTextColor(context.getResources().getColor(R.color.colorWhite));
                holder.cardLinearLayout.addView(infoTextView);
            }
            else if(topicContent.tagName() == "ol") {
                int cont = 1;

                for(Element listItem : topicContent.children()) {
                    TextView infoTextView = new TextView(context);
                    infoTextView.setText(cont++ + ". " + listItem.text());
                    infoTextView.setTextColor(context.getResources().getColor(R.color.colorWhite));
                    holder.cardLinearLayout.addView(infoTextView);
                }
            }
            else if(topicContent.tagName() == "span") {
                LinearLayout fileLinearLayout = new LinearLayout(context);
                fileLinearLayout.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                fileLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                fileLinearLayout.setGravity(Gravity.CENTER_VERTICAL);

                String iconName = topicContent.child(0).child(0).attr("src").split("/")[topicContent.child(0).child(0).attr("src").split("/").length - 1];

                ImageView icon = new ImageView(context);
                icon.setMaxHeight(40);
                icon.setMinimumHeight(40);
                icon.setMaxWidth(40);
                icon.setMinimumWidth(40);

                //Toast.makeText(context, iconName, Toast.LENGTH_LONG).show();

                if(iconName.equals("pdf.png")) {
                    icon.setImageResource(R.drawable.pdf);
                }
                else if(iconName.equals("doc.png")) {
                    icon.setImageResource(R.drawable.doc);
                }
                else if(iconName.equals("ppt.png")) {
                    icon.setImageResource(R.drawable.ppt);
                }
                else if(iconName.equals("tarefa.png")) {
                    icon.setImageResource(R.drawable.tarefa);
                }
                else if(iconName.equals("forumava.png")) {
                    icon.setImageResource(R.drawable.forumava);
                }
                else if(iconName.equals("zip.png")) {
                    icon.setImageResource(R.drawable.zip);
                }
                else if(iconName.equals("desconhecido.png")) {
                    icon.setImageResource(R.drawable.desconhecido);
                }
                else if(iconName.equals("video.png")) {
                    icon.setImageResource(R.drawable.video);
                }
                else if(iconName.equals("site_add.png")) {
                    icon.setImageResource(R.drawable.site_add);
                }

                fileLinearLayout.addView(icon);

                Button fileButton = new Button(context);
                //String iconUrl = topicContent.child(0).child(0).attr("src");
                fileButton.setText(topicContent.child(0).child(1).text());

                fileButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        formAva = topicContent.child(0).child(1).child(0).attr("id");

                        int index = topicContent.child(0).child(1).child(0).attr("onclick").indexOf("'id':'") + 6;
                        id = topicContent.child(0).child(1).child(0).attr("onclick").substring(index);

                        index = id.indexOf("'");
                        id = id.substring(0, index);

                        Toast.makeText(context, "Baixando arquivo.", Toast.LENGTH_SHORT).show();
                        //new FileDownloader().execute();

                        String url = "https://sigaa.ufpi.br/sigaa/ava/index.jsf?formAva=formAva&formAva:idTopicoSelecionado=0&javax.faces.ViewState=j_id8&" + formAva + "=" + formAva + "&id=" + id;
                        Log.i("File URL", url);

                        fileDownloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                        queueId = fileDownloadManager.enqueue(request);

                        /*Intent downloads = new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);
                        context.startActivity(downloads);*/
                    }
                });

                fileLinearLayout.addView(fileButton);

                holder.cardLinearLayout.addView(fileLinearLayout);

                /*LinearLayout file = new LinearLayout(context);
                file.setOrientation(LinearLayout.HORIZONTAL);*/

                /*String iconName = topicContent.child(0).child(0).attr("src").split("/")[topicContent.child(0).child(0).attr("src").split("/").length - 1];

                ImageView icon = new ImageView(context);
                icon.setMaxHeight(40);
                icon.setMinimumHeight(40);
                icon.setMaxWidth(40);
                icon.setMinimumWidth(40);

                //Toast.makeText(context, iconName, Toast.LENGTH_LONG).show();

                if(iconName.equals("pdf.png")) {
                    icon.setImageResource(R.drawable.pdf);
                }
                else if(iconName.equals("doc.png")) {
                    icon.setImageResource(R.drawable.doc);
                }
                else if(iconName.equals("ppt.png")) {
                    icon.setImageResource(R.drawable.ppt);
                }
                else if(iconName.equals("tarefa.png")) {
                    icon.setImageResource(R.drawable.tarefa);
                }
                else if(iconName.equals("forumava.png")) {
                    icon.setImageResource(R.drawable.forumava);
                }
                else if(iconName.equals("zip.png")) {
                    icon.setImageResource(R.drawable.zip);
                }
                else if(iconName.equals("desconhecido.png")) {
                    icon.setImageResource(R.drawable.desconhecido);
                }
                else if(iconName.equals("video.png")) {
                    icon.setImageResource(R.drawable.video);
                }
                else if(iconName.equals("site_add.png")) {
                    icon.setImageResource(R.drawable.site_add);
                }

                holder.cardLinearLayout.addView(icon);*/
                /*TextView fileTextView = new TextView(context);
                fileTextView.setText(topicContent.child(0).child(1).text());

                file.addView(icon);
                //file.addView(fileTextView);

                holder.cardLinearLayout.addView(file);*/
            }
        }
    }

    @Override
    public int getItemCount() {
        return topicsList.size();
    }

    @Override
    public void onViewRecycled(@NonNull TopicViewHolder holder) {
        super.onViewRecycled(holder);
        holder.cardLinearLayout.removeAllViews();
    }

    class TopicViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        LinearLayout cardLinearLayout;

        public TopicViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.topic_layout_name);
            cardLinearLayout = (LinearLayout) itemView.findViewById(R.id.topic_layout_card);

            /*Document document = Utils.requestedPage;
            Element topic = document.getElementsByClass("topico-aula").get(getItemCount() - 1);

            for(Element topicContent : topic.getElementsByClass("conteudotopico").first().children()) {
                if(topicContent.tagName() == "p") {
                    //Log.i(topic.getElementsByClass("titulo").first().text(), topicContent.text());

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    TextView infoTextView = new TextView(context);
                    infoTextView.setLayoutParams(layoutParams);
                    infoTextView.setText(topicContent.text());
                    cardLinearLayout.addView(infoTextView);
                }
                else if(topicContent.tagName() == "span") {
                    Button fileButton = new Button(context);
                    fileButton.setText(topicContent.child(0).child(1).text());
                }
            }*/
        }

    }

    public class FileDownloader extends AsyncTask<Void, Void, Document> {

        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress = new ProgressDialog(context);
            progress.setMessage("Baixando arquivo...");
            progress.show();
        }

        @Override
        protected Document doInBackground(Void... params) {
            /*Document document = null;

            try {
                Connection.Response response = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ava/index.jsf?formAva=formAva&formAva:idTopicoSelecionado=0&javax.faces.ViewState=" + javaxFaces + "&" + formAva + "=" + formAva + "&id=" + id)
                        .method(Connection.Method.GET)
                        .execute();
                document = response.parse();
                Log.i("URL", "" + response.url());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return document;*/

            /*try {
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                File file = new File(path, "_Ascent_Test.txt");

                URL url = new URL("https://sigaa.ufpi.br/sigaa/ava/index.jsf?formAva=formAva&formAva:idTopicoSelecionado=0&javax.faces.ViewState=" + javaxFaces + "&" + formAva + "=" + formAva + "&id=" + id);
                InputStream in = null;
                in = url.openStream();

                OutputStream out = new BufferedOutputStream(new FileOutputStream(path + "testeAppUfpi"));

                for (int b; (b = in.read()) != -1;) {
                    out.write(b);
                }
                out.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL("https://sigaa.ufpi.br/sigaa/ava/index.jsf?formAva=formAva&formAva:idTopicoSelecionado=0&javax.faces.ViewState=" + javaxFaces + "&" + formAva + "=" + formAva + "&id=" + id);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                /*if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();*/

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream("/sdcard/testAppUfpi");

                byte data[] = new byte[4096];
                int count;
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                //return e.toString();
                e.printStackTrace();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Document document) {
            //Log.i("File Downloader", "" + document);

            progress.setMessage("Arquivo baixado...");
            progress.dismiss();
        }
    }

}
