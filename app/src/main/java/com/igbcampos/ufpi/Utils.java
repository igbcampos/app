package com.igbcampos.ufpi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class Utils {
    public static Document homepage = null;
    public static Document requestedPage = null;
    public static Connection.Response connectionResponse = null;
    public static String operation = "";
    public static String fileName = "userData";
    public static String user = "";
    public static String password = "";
    public static String classId;
    public static Boolean invalidInfo = false;
    public static Boolean noConnection = false;
    public static Context viewContext;

    public Utils() {
        try {
            new Scraper().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public Utils(Context context) {
        viewContext = context;

        try {
            File file = new File(context.getFilesDir(), fileName);

            BufferedReader input = new BufferedReader (new InputStreamReader(new FileInputStream(file)));
            String line;
            StringBuffer buffer = new StringBuffer ();
            while((line = input.readLine ()) != null) {
                buffer.append(line);
            }

            user = buffer.toString().split("-%")[0];
            password = buffer.toString().split("-%")[1];

            new Scraper().execute().get();
        }
        catch(IOException e) {
            e.printStackTrace ();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void createFile(Context context, String data) {
        try {
            FileOutputStream outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void readFile(Context context) {
        try {
            File file = new File(context.getFilesDir(), fileName);

            BufferedReader input = new BufferedReader (new InputStreamReader(new FileInputStream(file)));
            String line;
            StringBuffer buffer = new StringBuffer ();
            while((line = input.readLine ()) != null) {
                buffer.append(line);
            }

            user = buffer.toString().split("-%")[0];
            password = buffer.toString().split("-%")[1];
        }
        catch(IOException e) {
            e.printStackTrace ();
        }
    }

    public static void deleteFile(Context context) {
        File file = new File(context.getFilesDir(), fileName);
        boolean deleted = file.delete();
        Log.i("File deleted", "" + deleted);
    }

    public static void startWork() {
        Data data = new Data.Builder()
                .putString("user", user)
                .putString("password", password)
                .build();

        /*Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();*/

        final PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(AuthenticationWorker.class, 30, TimeUnit.MINUTES)
                .addTag("periodic_work")
                .setInputData(data)
                //.setConstraints(constraints)
                .build();

        if (WorkManager.getInstance() != null) {
            WorkManager.getInstance().enqueue(periodicWorkRequest);
        }
    }



    public static String getWifiName() {
        WifiManager manager = (WifiManager) viewContext.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return null;
    }



    public static void stopWork() {
        if (WorkManager.getInstance() != null) {
            WorkManager.getInstance().cancelAllWorkByTag("periodic_work");
        }
    }

    public void noConnectionWarning() {
        AlertDialog alertDialog = new AlertDialog.Builder(viewContext).create();
        alertDialog.setTitle("Sem conexão");
        alertDialog.setMessage("Por favor, verifique sua conexão a internet e tente novamente.");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "TENTAR NOVAMENTE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            new Scraper().execute().get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void invalidInfoWarning() {
        Utils.deleteFile(viewContext);

        AlertDialog alertDialog = new AlertDialog.Builder(viewContext).create();
        alertDialog.setTitle(R.string.invalid_data);
        alertDialog.setMessage("Usuário e/ou senha inválidos.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void authenticate(Context context) {
        new Authenticator().execute();
    }

    public void sendData(Context context) {
        new ApiFetcher().execute();
    }

    //AsyncTask class that perform general requests within Sigaa
    public class Scraper extends AsyncTask<Void, Void, Document> {
        ProgressBar progressBar;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progressBar = new ProgressBar(viewContext);
            progressBar.setVisibility(View.VISIBLE);*/
            progress = new ProgressDialog(viewContext);
            progress.setMessage("Acessando dados...");
            progress.show();
        }

        @Override
        protected Document doInBackground(Void... params) {
            Document document = null;

            try {
                Connection.Response response = Jsoup.connect("https://sigaa.ufpi.br/sigaa/logar.do?dispatch=logOn&user.login=" + user + "&user.senha=" + password)
                        .method(Connection.Method.GET)
                        .execute();
                document = response.parse();
                homepage = document;
                if(document.getElementsByClass("sair-sistema").toString().length() > 0) {
                    /*if(operation == "home") {
                        homepage = document;
                    }
                    else*/
                    if(operation == "class") {
                        document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?form_acessarTurmaVirtual=form_acessarTurmaVirtual&idTurma=" + classId + "&javax.faces.ViewState=j_id1&form_acessarTurmaVirtual%3AturmaVirtual=form_acessarTurmaVirtual%3AturmaVirtual")
                                .data("cookieexists", "false")
                                .cookies(response.cookies())
                                .post();
                        requestedPage = document;
                    }
                    else if(operation == "grades") {
                        document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?menu%3Aform_menu_discente=menu%3Aform_menu_discente&id=150244&jscook_action=menu_form_menu_discente_j_id_jsp_1325243614_85_menu%3AA%5D%23%7B+relatorioNotasAluno.gerarRelatorio+%7D&javax.faces.ViewState=j_id1")
                                .data("cookieexists", "false")
                                .cookies(response.cookies())
                                .post();
                        requestedPage = document;
                    }
                    else if(operation == "library") {
                        document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?menu%3Aform_menu_discente=menu%3Aform_menu_discente&id=150244&jscook_action=menu_form_menu_discente_j_id_jsp_1325243614_85_menu%3AA%5D%23%7BmeusEmprestimosBibliotecaMBean.iniciarVisualizarEmprestimosRenovaveis%7D&javax.faces.ViewState=j_id1")
                                .data("cookieexists", "false")
                                .cookies(response.cookies())
                                .post();
                        requestedPage = document;

                        if(Utils.requestedPage.getElementsByClass("warning").size() == 0) {
                            String javaxFaces = requestedPage.getElementById("javax.faces.ViewState").attr("value");
                            String books = "";

                            for(Element book : requestedPage.getElementsByClass("listagem").first().child(2).children()) {
                                books += "&" + book.child(0).child(0).attr("name") + "=on";
                            }

                            String url = "https://sigaa.ufpi.br/sigaa/biblioteca/circulacao/renovaMeusEmprestimos.jsf?formularioRenovamaMeusEmprestimos=formularioRenovamaMeusEmprestimos" +
                                    "&formularioRenovamaMeusEmprestimos:inputHiddenMensagemPaginaVisualiza=formularioRenovamaMeusEmprestimos:inputHiddenMensagemPaginaVisualiza" +
                                    books + "&formularioRenovamaMeusEmprestimos:cmdButtonConfirmarRenovacaoMeusEmprestimos=Renovar Empréstimos Selecionados" +
                                    "&javax.faces.ViewState=" + javaxFaces;

                            document = Jsoup.connect(url)
                                    .data("cookieexists", "false")
                                    .cookies(response.cookies())
                                    .post();

                            requestedPage = document;

                            Log.i("BIB", url);
                        }
                    }
                    else if(operation == "schedule") {
                        //document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?menu%3Aform_menu_discente=menu%3Aform_menu_discente&id=150244&jscook_action=menu_form_menu_discente_j_id_jsp_1325243614_85_menu%3AA%5D%23%7BmeusEmprestimosBibliotecaMBean.iniciarVisualizarEmprestimosRenovaveis%7D&javax.faces.ViewState=j_id1")
                        //document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?menu%3Aform_menu_discente=menu%3Aform_menu_discente&id=150244&jscook_action=menu_form_menu_discente_j_id_jsp_1325243614_85_menu%3AA%5D%23%7BmatriculaGraduacaoUFPI.verComprovanteSolicitacoes%7D&javax.faces.ViewState=j_id10")
                        //document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/graduacao/matricula/comprovante_solicitacoes.jsf")
                        String javaFaces = homepage.getElementById("javax.faces.ViewState").attr("value");
                        document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/ufpi/portais/discente/discente.jsf?menu%3Aform_menu_discente=menu%3Aform_menu_discente&id=150244&jscook_action=menu_form_menu_discente_j_id_jsp_1325243614_85_menu%3AA%5D%23%7B+portalDiscente.atestadoMatricula+%7D&javax.faces.ViewState=" + javaFaces)
                                //.data("cookieexists", "false")
                                .cookies(response.cookies())
                                .post();
                        requestedPage = document;

                        /*if(document.getElementsByClass("erros").size() > 0 && document.getElementsByClass("erros").first().child(0).text().equals("O discente selecionado não possui matrículas em componentes do período atual.")) {
                            document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/graduacao/matricula/comprovante_solicitacoes.jsf")
                                    .data("cookieexists", "false")
                                    .cookies(response.cookies())
                                    .post();
                            requestedPage = document;
                        }*/
                    }
                    else if(operation == "bond") {
                        document = Jsoup.connect("https://sigaa.ufpi.br/sigaa/escolhaVinculo.do?dispatch=listar")
                                .data("cookieexists", "false")
                                .cookies(response.cookies())
                                .post();
                        requestedPage = document;

                        for(Element status : document.getElementsByClass("subFormulario")) {
                            for(Element line : status.child(2).children()) {
                                if(line.children().size() > 0) {
                                    String link = line.child(0).child(0).attr("href");
                                    String bond = line.child(1).child(0).text();
                                    String id = line.child(2).child(0).text();
                                    String activate = line.child(3).child(0).text();
                                    String major = line.child(4).child(0).text();

                                    Log.i("Bond", link + ", " + bond + ", " + id + ", " + activate + ", " + major);
                                }
                                else {
                                    Log.i("Bond", "Linha vazia.");
                                }
                            }
                        }
                    }

                    invalidInfo = false;
                }
                else {
                    invalidInfo = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
            progress.setMessage("Dados carregados...");
            progress.dismiss();
            noConnection = false;
            //progressBar.setVisibility(View.GONE);

            if(document == null) {
                noConnection = true;
                noConnectionWarning();
            }
            else if(invalidInfo) {
                invalidInfoWarning();
            }
        }
    }

    //AsyncTask class responsible for the user authentication in the network.
    public class Authenticator extends AsyncTask<Void, Void, Document> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Document doInBackground(Void... params) {
            Document document = null;

            try {
                Connection.Response response = Jsoup.connect("https://sigaa.ufpi.br/sigaa/logar.do?dispatch=logOn&user.login=" + user + "&user.senha=" + password)
                        .method(Connection.Method.GET)
                        .execute();
                document = response.parse();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
            noConnection = false;

            if(document == null) {
                noConnection = true;
                noConnectionWarning();
            }
        }
    }

    //AsyncTask class responsible for the user authentication in the network.
    public class ApiFetcher extends AsyncTask<Void, Void, Document> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Document doInBackground(Void... params) {
            Document document = null;

            try {
                String json = "{\"user\": \"" + user + "\", \"password\": \"" + password + "\"}";

                Connection.Response response = Jsoup.connect("http://igbcampos.pythonanywhere.com/api/user/")
                        .method(Connection.Method.POST)
                        .header("Content-Type", "application/json")
                        .requestBody(json)
                        .execute();
                document = response.parse();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
        }
    }

}
