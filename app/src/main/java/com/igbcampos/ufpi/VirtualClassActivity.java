package com.igbcampos.ufpi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public class VirtualClassActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_class);

        if(Utils.requestedPage != null) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.student_virtual_class_activity_recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            //initializing the productlist
            List<Topic> topicsList = new ArrayList<>();

            Document document = Utils.requestedPage;

            //String className = document.getElementById("linkCodigoTurma").text();
            String className = document.getElementById("linkNomeTurma").text();
            //className += " " + document.getElementById("linkPeriodoTurma").text();

            getSupportActionBar().setTitle(className/*.split(" ")[0]*/);
            TextView nameTextView = (TextView) findViewById(R.id.student_virtual_class_activity_name);
            nameTextView.setText(className);
            nameTextView.setVisibility(View.GONE);

            String content = "";
            TextView contentTextView = (TextView) findViewById(R.id.student_virtual_class_activity_content);

            if(document.getElementsByClass("topico-aula").size() == 0) {
                contentTextView.setText("Ainda não há materiais disponíveis nessa turma.");

                RecyclerView topicsRecyclerView = (RecyclerView) findViewById(R.id.student_virtual_class_activity_recycler_view);
                topicsRecyclerView.setVisibility(View.GONE);
            }
            else {
                contentTextView.setVisibility(View.GONE);

                for(Element topic : document.getElementsByClass("topico-aula")) {
                    //content += "\n\n" + topic.getElementsByClass("titulo").first().text();
                    topicsList.add(new Topic(topic.getElementsByClass("titulo").first().text()));

                /*for(Element topicContent : topic.getElementsByClass("conteudotopico").first().children()) {
                    if(topicContent.tagName() == "p") {
                        content += "\n      " + topicContent.text();
                    }
                    else if(topicContent.tagName() == "span") {
                        Button fileButton = new Button(this);
                        fileButton.setText(topicContent.child(0).child(1).text());

                        //content += "\n      " + topicContent.child(0).child(1).text();
                    }
                }*/
                }

                //creating recyclerview adapter
                TopicAdapter adapter = new TopicAdapter(this, topicsList);

                //setting adapter to recyclerview
                recyclerView.setAdapter(adapter);
            }
        }
    }
}
